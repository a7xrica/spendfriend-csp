import './App.css'

import {useState, useEffect} from 'react'

import Navbar from 'components/AppNavBar'
import Home from 'pages/home'
import Register from 'pages/register'
import Login from 'pages/login'
import AddCategories from 'pages/category'
import Income from 'pages/income'
import Expense from 'pages/expense'
import Logout from 'pages/logout'
import Profile from 'pages/profile'

import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'
import {Container} from 'react-bootstrap'

import {UserProvider} from './userContext'

function App() {

  const [user, setUser] = useState({

    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === "true"

  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return(

    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <Navbar />
            <Container>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/profile" component={Profile} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/category" component={AddCategories} />
              <Route exact path="/income" component={Income} />
              <Route exact path="/expense" component={Expense} />
              <Route exact path="/logout" component={Logout} />
            </Switch>
          </Container>
        </Router>
      </UserProvider>
    </>

    )
}

export default App;