import React, {useState, useEffect, useContext} from 'react'
import {Form, Button, Table} from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from 'userContext'
import {Redirect} from 'react-router-dom'

export default function AddCategories(){

	const {user} = useContext(UserContext)

	const [name,setName] = useState("")
	const [type,setType] = useState("")
	const [isComplete, setIsComplete] = useState("")

	const [allCategories, setAllCategories] = useState([])
	const isLogged = !!localStorage.getItem('token');


	useEffect(()=> {

		if ((name !== "") && (type !== "")) {
			setIsComplete(true)
		} else {
			setIsComplete(false)
		}

	},[type, name])

	useEffect(()=> {

		if (isLogged) {
			fetch('https://obscure-ocean-86502.herokuapp.com/api/categories', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res=>res.json())
			.then(data=>{
				//console.log(data)
				setAllCategories(data.map(category=> {
					return (

						<tr>
							<td>{category.name}</td>
							<td>{category.type}</td>
							<td><Button variant="success" type="submit" onClick={() => {deleteCategory(category._id) }}>Delete</Button></td>
						</tr>)
				}))
			})
		}
	}, [user, allCategories])

	function addCategory(e) {

		e.preventDefault()

		let token = localStorage.getItem('token')

		fetch('https://obscure-ocean-86502.herokuapp.com/api/categories/', {

			method: 'POST',
			headers: {
				"Content-Type": "application/JSON",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				type: type
			})
		})
		.then(res=> res.json())
		.then(data=> {
			console.log(data)
			if (data.error === "Category already exists.") {
				Swal.fire({
					icon: "error",
					title: "Cannot Add Category"
				})
			} else {
				Swal.fire({
					icon: "success",
					title: "New Category Added!"
				})
			}
		})

		setName("")
		setType("")

	}

	function deleteCategory(id) {

		fetch(`https://obscure-ocean-86502.herokuapp.com/api/categories/${id}`, {

			method: 'DELETE',
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then (res=> res.json())
		.then(data=>{
			console.log(data)
		})
	}

	return (

		isLogged
		?
		<div className="fullPage">
		<div>
			<h2 className="text-center">Categories</h2>
			<h3 className="text-center">Add Category</h3>
			<Form onSubmit={e=> addCategory(e)}>
				<Form.Group>
					<Form.Label>Category Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Category Name" value={name} onChange={e=>{setName(e.target.value)}} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Type</Form.Label>
					<Form.Control as="select" placeholder="Enter Category Type" value={type} onChange={e=>{setType(e.target.value)}} required>
						<option value="" disabled>Select Category Type</option>
						<option>Income</option>
						<option>Expense</option>
					</Form.Control>
				</Form.Group>
				{
					isComplete
					?
					<Button variant="success" type="submit">Submit</Button>
					:
					<Button variant="success" type="submit" disabled>Submit</Button>
				}
			</Form>
		</div>
		<br />
		<br />
		<>
			<h4 className="text-center">Your Categories</h4>
			<Table bordered hover>
				<thead>
					<tr>
						<th>Name</th>
						<th>Type</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{allCategories}
				</tbody>
			</Table>
		</>
		</div>
		:
		<Redirect to="/login" /> 

		)

}