import React, {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from 'userContext'

export default function Register(){

	const {user,setUser} = useContext(UserContext)

	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [isAdmin,setIsAdmin] = useState(false)
	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword,setConfirmPw] = useState("")

	const [isComplete, setIsComplete] = useState(false)

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && password !== "") && (password.length >= 8) && (password === confirmPassword)) {

			setIsComplete(true)

		} else {

			setIsComplete(false)
		}

	},[firstName,lastName,email,password,confirmPassword])

	function register(e){

		e.preventDefault(e)

		fetch('https://obscure-ocean-86502.herokuapp.com/api/users/', {

			method: 'POST',
			headers: {
				"Content-Type": "application/JSON"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password,
				confirmPassword: confirmPassword
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			Swal.fire({

				icon: "success",
				title: "Registration Complete!",
				text: "Thank you for registering to Zuitt Booking System."

			})
		})

		// clear out the states to their initial values 
		setFirstName("")
		setLastName("")
		setEmail("")
		setPassword("")
		setConfirmPw("")
	}


	return (

		user.email
		?
		<Redirect to="/profile" /> 
		:
		<div className="divLogin">
			<h2 className="text-center">Register</h2>
			<Form onSubmit={e=> register(e)}>
				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e=>{setFirstName(e.target.value)}} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e=>{setLastName(e.target.value)}} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Email</Form.Label>
					<Form.Control type="text" placeholder="Enter Your Email" value={email} onChange={e=>{setEmail(e.target.value)}} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter Your Password" value={password} onChange={e=>{setPassword(e.target.value)}} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" value={confirmPassword} onChange={e=>{setConfirmPw(e.target.value)}} required />
				</Form.Group>
				{
					isComplete
					?
					<Button variant="primary" type="submit">Submit</Button>
					:
					<Button variant="primary" type="submit" disabled>Submit</Button>
				}
			</Form>
		</div>

		)
}