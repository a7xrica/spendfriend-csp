import React, {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from 'userContext'

export default function Login() {

	const {user,setUser} = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [completeLogin, setCompleteLogin] = useState(false)

	const [redirect,setRedirect] = useState(false)

	useEffect(()=>{

		if(email === "" && password === "") {

			setCompleteLogin(false)

		} else {

			setCompleteLogin(true)
		}

	},[email,password])


	function login(e){

		e.preventDefault()

		fetch('https://obscure-ocean-86502.herokuapp.com/api/users/login', {

			method: 'POST',
			headers: {
				"Content-Type": "application/JSON"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data.accessToken){
				localStorage.setItem('token',data.accessToken)
			Swal.fire({

				icon: "success",
				title: "Logged In Successfully!",
				text: "Thank you for logging in to SpendFriend Budget Tracking App."

			})

			fetch('https://obscure-ocean-86502.herokuapp.com/api/users/', {

				headers: {
					Authorization: `Bearer ${data.accessToken}`
				}
			})
			.then(res => res.json())
			.then(data => {
				localStorage.setItem('email',data.email)
				localStorage.setItem('isAdmin',data.isAdmin)

				setRedirect(true)

				setUser({
					email: data.email,
					isAdmin: data.isAdmin
				})
			})
		} else {
			Swal.fire({
				icon:"error",
				title: "Login failed."
			})
			
		}

		setEmail("")
		setPassword("")
		})
	}


	return (

		user.email
		?
		<Redirect to="/profile" /> 
		:
		redirect
		?
		<Redirect to="/profile" /> 
		:
		<div className="divLogin">
			<h2 className="text-center">Login</h2>
			<Form onSubmit={e=> login(e)}>
				<Form.Group>
					<Form.Label>Email</Form.Label>
					<Form.Control type="text" placeholder="Enter your email" value={email} onChange={e=>{setEmail(e.target.value)}} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter your password" value={password} onChange={e=>{setPassword(e.target.value)}} required/>
				</Form.Group>
				{
					completeLogin
					?
					<Button variant="primary" type="submit">Login</Button>
					:
					<Button variant="primary" type="submit" disabled>Login</Button>
				}
				
			</Form>
		</div>

		)
}