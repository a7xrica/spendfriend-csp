import React, {useState, useEffect, useContext} from 'react'
import {Form, Button, Table} from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from 'userContext'
import {Redirect} from 'react-router-dom'

export default function Expense() {

	const {user} = useContext(UserContext)

	const [category, setCategory] = useState("")
	const [amount, setAmount] = useState(0)
	const [entries, setEntries] = useState([])

	const [totalAmount, setTotalAmount] = useState(0)
	const [allCategories, setAllCategories] = useState([])
	const [completeEntry, setCompleteEntry] = useState(false)

	const isLogged = !!localStorage.getItem('token')

	useEffect(()=>{

		if(amount !== "") {

			setCompleteEntry(true)

		} else {

			setCompleteEntry(false)
		}

	},[amount])

	useEffect(()=> {
		if(isLogged) {
			fetch('https://obscure-ocean-86502.herokuapp.com/api/entries/', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res=>res.json())
			.then(data=>{
				//console.log(data)
				setTotalAmount(data.filter(entry=> entry.type === "Expense").reduce((x,y)=> x+y.amount, 0))
			})
		}
	}, [user, totalAmount, entries])

	useEffect(()=> {

		if(isLogged) {
			fetch('https://obscure-ocean-86502.herokuapp.com/api/categories', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res=>res.json())
			.then(data=> {
				//console.log(data)
				setAllCategories(data.map(category=> {
					return (

						category.type === "Expense"
						?
						<>
							<option>{category.name}</option>
						</>
						:
						<></>

						)
				}))
			})
		} else {
			return <Redirect to="/login" /> 
		}
	}, [user])

	useEffect(()=> {

		if(isLogged) {
			fetch('https://obscure-ocean-86502.herokuapp.com/api/entries', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res=> res.json())
			.then(data=> {
				setEntries(data.map(entry=> {
					return (

						entry.type === "Expense"
						?
						<>
							<tr>
								<td>{entry.category}</td>
								<td>{entry.amount}</td>
								<td>
									<Button variant="success" type="submit" onClick={() => {deleteEntry(entry._id) }}>Delete</Button>
								</td>
							</tr>
						</>
						:
						<></>

						)
				}))
			})
		}
	}, [user, entries])


	function addExpenseEntry(e) {

		e.preventDefault()

		let token = localStorage.getItem('token')

		fetch('https://obscure-ocean-86502.herokuapp.com/api/entries/', {

			method: 'POST',
			headers: {
				"Content-Type": "application/JSON",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				category: category,
				amount: amount,
				type: "Expense"
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			if(data){
				Swal.fire({
					text: "Entry added successfully."
				})
			}
		})

		setCategory("")
		setAmount(0)

	}

	function deleteEntry(id) {

		fetch(`https://obscure-ocean-86502.herokuapp.com/api/entries/${id}`, {

			method: 'DELETE',
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then (res=> res.json())
		.then(data=>{
			console.log(data)
		})
	}

	return (

		isLogged
		?
		<div className="fullPage">
			<h2 className="text-center">Expense Tracker</h2>
			<h3 className="text-center">Add Record</h3>
			<Form onSubmit={e=> addExpenseEntry(e)}>
				<Form.Group>
					<Form.Label>Category:</Form.Label>
					<Form.Control as="select" placeholder="Select Category" value={category} onChange={e=>{setCategory(e.target.value)}} required>
						<option value="" disabled>Select Category</option>
						{allCategories}
					</Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Amount:</Form.Label>
					<Form.Control type="number" placeholder="Enter Category Name" value={amount} onChange={e=>{setAmount(e.target.value)}} required />
				</Form.Group>
				{
					completeEntry
					?
					<Button variant="success" type="submit">Submit</Button>
					:
					<Button variant="success" type="submit" disabled>Submit</Button>
				}
			</Form>
			<br />
			<br />
			<>
				<h4 className="text-center">Expense</h4>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Name</th>
							<th>Amount</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{entries}
						<tr>
							<td style={{backgroundColor: "#355d39", color: "#f7f2f2"}}>Total:</td>
							<td style={{backgroundColor: "#355d39", color: "#f7f2f2"}}>{totalAmount}</td>
						</tr>
					</tbody>
				</Table>
			</>
		</div>
		:
		<Redirect to="/login" /> 
		)
}