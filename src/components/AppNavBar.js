import React, {useContext} from 'react'
import {Navbar,Nav} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'

import UserContext from 'userContext'

export default function NavBar() {

	const {user} = (useContext(UserContext))
	console.log(user)

	return (

		<div id="fixed">
		<Navbar expand="lg" id="nav">
			<Navbar.Brand href="/">SpendFriend</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				{
					user.email
					?
					<>
						<Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>
						<Nav.Link as={NavLink} to="/category">Categories</Nav.Link>
						<Nav.Link as={NavLink} to="/income">Income</Nav.Link>
						<Nav.Link as={NavLink} to="/expense">Expenses</Nav.Link>
						<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
					</>
					:
					<>
						<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
						<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
					</>
				}
			</Navbar.Collapse>
		</Navbar>
		</div>

		)
}